import {createRouter, createWebHistory} from 'vue-router';

import HomeView from '../views/HomeView.vue';
import RegisterView from "@/views/RegisterView.vue";
import LoginView from "@/views/LoginView.vue";
import LessonView from "@/views/lesson/LessonsView.vue";
import ManageView from "@/views/ManageView.vue";
import LessonOneView from "@/views/lesson/LessonOneView.vue";
import LessonThreeView from "@/views/lesson/LessonThreeView.vue";
import LessonTwoView from "@/views/lesson/LessonTwoView.vue";
import LessonFourView from "@/views/lesson/LessonFourView.vue";
import NotFound from "@/views/status/NotFound.vue";

import GameView from "@/views/game/GameView.vue";
import Dev from "@/Dev.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {path: '/', name: 'home', component: HomeView},
        {path: '/register', name: 'register', component: RegisterView},
        {path: '/login', name: 'login', component: LoginView},
        {path: '/lessons', name: 'lessons', component: LessonView},
        {path: '/game', name: 'game', component: GameView},
        {path: '/manage', name: 'manage', component: ManageView},
        {path: '/lesson1', name: 'lesson1', component: LessonOneView},
        {path: '/lesson2', name: 'lesson2', component: LessonTwoView},
        {path: '/lesson3', name: 'lesson3', component: LessonThreeView},
        {path: '/lesson4', name: 'lesson4', component: LessonFourView},
        {path: '/dev', name: 'dev', component: Dev},
        {path: '/:pathMatch(.*)*', component: NotFound},
    ],
});

export default router;
