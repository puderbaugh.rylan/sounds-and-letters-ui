export interface QuestionAnswer {
    id: string,
    question: string,
    answer: string,
    audioPath: string
}