import './assets/main.css'

import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import App from './App.vue'
import Router from "@/router/router.js";

createApp(App).use(ElementPlus).use(Router).mount('#app')
