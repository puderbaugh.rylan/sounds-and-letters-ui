# Sounds & Letters

Web application built with Vue 3 in Vite.

## Dependencies
```
nodejs
npm
```
### Install nodejs and npm

### Linux
`Debian/Ubuntu` \
sudo apt update \
sudo apt install nodejs npm

`Arch` \
sudo pacman -Syu \
sudo pacman -S nodejs npm

`OpenSUSE` \
zypper install nodejs14

To check the version type: \
```
nodejs --version
npm --version
```

### Windows
Download the installer: https://nodejs.org/en/#home-downloadhead

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

## Customize Configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).
